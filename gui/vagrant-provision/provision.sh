#!/bin/sh

# Node and npm
sudo apt update
cd /vagrant
sudo apt install -y nodejs npm
sudo npm install -g n
sudo n stable
sudo apt purge -y nodejs npm
sudo npm install
# If you are using Mac, it is sufficient here.

# Because 'command not found: ng' occurs on Windows, add this command.
sudo npm install -g @angular/cli
