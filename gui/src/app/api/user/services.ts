import { Injectable } from "@angular/core";
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';

import { User } from './models';
import { SysConfigs } from '../api.config';

@Injectable()
export class ApiUserService {
	private url: string = SysConfigs.baseUrl + '/api/users';
	private headers = new Headers({ 'Content-Type': 'application/json' });

	constructor(
		private http: Http
	){}

	// Get all links
	getAllUsers(): Promise<User[]> {
		return this.http
			.get(this.url)
			.toPromise()
			.then(response => response.json())
			.catch(err => {
				return Promise.reject(err.json().error);
			})
	}
}