export class User {
	id: number;
	name: string;
	pass: string;
	email: string;
	authority: number;
	updated_at: string;
	created_at: string;
}
